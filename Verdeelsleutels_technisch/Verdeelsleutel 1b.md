---
title: 1b. Verdeelsleutel gedeclareerd bedrag per langdurige zorg sector
weight: 1
---
## SPARQL query
```
# Verdeelsleutel: 1b
# Parameters: -
# Ontologie: versie 2.0.0

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT
    (SUM(?ggzbAmount) / SUM(?amount) *100 as ?GGZB)
    (SUM(?lgAmount) / SUM(?amount) *100 as ?LG)
    (SUM(?lvgAmount) / SUM(?amount) *100 as ?LVG)
    (SUM(?vgAmount) / SUM(?amount) *100 as ?VG)
    (SUM(?zgaudAmount) / SUM(?amount) *100 as ?ZGAUD)
    (SUM(?zgvisAmount) / SUM(?amount) *100 as ?ZGVIS)
    (SUM(?vvAmount) / SUM(?amount) *100 as ?VV)
    
WHERE {
    BIND("2022-01-01"^^xsd:date AS ?start_periode)
    BIND("2022-12-01"^^xsd:date AS ?eind_periode)
    ?declaratie
        a onz-g:CareInvoice ;
        onz-g:hasDate ?date ;
        onz-g:isAbout [
            a onz-g:FinancialEntity ;
            onz-g:hasQuality [onz-g:hasQualityValue [onz-g:hasDataValue ?amount]]
        ] ;
    onz-g:isAbout ?zorgproces .
    FILTER(?start_periode <= ?date && ?date <= ?eind_periode)
    
    ?zorgproces
        a onz-zorg:NursingProcess ;
        onz-g:definedBy ?indicatie .
    ?indicatie
        a onz-zorg:WlzIndicatie ;
        onz-g:hasPart/onz-g:isAbout ?sector .

    #Per financieringsstroom
    BIND(IF(?sector = onz-zorg:GGZ-B, ?amount, 0) AS ?ggzbAmount)
    BIND(IF(?sector = onz-zorg:LG, ?amount, 0) AS ?lgAmount)
    BIND(IF(?sector = onz-zorg:LVG, ?amount, 0) AS ?lvgAmount)
    BIND(IF(?sector = onz-zorg:VG, ?amount, 0) AS ?vgAmount)
    BIND(IF(?sector = onz-zorg:ZGAUD, ?amount, 0) AS ?zgaudAmount)
    BIND(IF(?sector = onz-zorg:ZGVIS, ?amount, 0) AS ?zgvisAmount)
    BIND(IF(?sector = onz-zorg:VV, ?amount, 0) AS ?vvAmount)
  
} GROUP BY ?sector
```