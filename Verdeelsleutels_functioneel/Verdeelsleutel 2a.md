---
title: 2a. Verdeelsleutel duur zorglegitimatie per wet.
weight: 3
---

## Toelichting

Deze verdeelsleutel betreft de procentuele verhouding van de duur van het traject op basis van de Wlz-indicatie, het DBC-traject of de Wmo-beschikking bij een of meerdere vestigingen in de verslagperiode. De verdeelsleutel wordt berekend per wet, op organisatieniveau en per vestiging over een nader te bepalen periode.


## Uitgangspunten

* De duur wordt bepaald op basis van de periode waarin het recht (ook wel legitimatie) bestaat om zorg te leveren. 
* De periode wordt bepaald o.b.v. het aantal dagen dat ligt tussen de start- en einddatum van de Wlz-indicatie, het DBC-traject en/of de Wmo-beschikking van een cliënt.


## Berekening

De verdeelsleutel wordt als volgt berekend:

1. Selecteer alle clienten die in de verslagperiode beschikken over een Wlz-indicatie en/of Zvw-traject en/of Wmo-beschikking of overige trajecten. 
2. Bepaal per client uit stap 1 de vestiging(en) en de wet waaronder de Wlz-indicatie(s) en/of Zvw-trajecten en/of Wmo-beschikkingen en/of overige trajecten vallen. 
3. Bepaal per vestiging en voor de totale organisatie het aantal dagen dat het traject duurde in de verslagperiode per wet en de wetten tezamen.
4. Bereken het percentage, door per vestiging het aantal dagen per wet te delen door het totaal aantal dagen van de wetten per vestiging tezamen en te vermeinigvuldigen met 100%.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj
| Verderling duur traject: | Wlz | Zvw | Wmo | Overig |
|----------------|--------|-----------|--------|----|
| Totaal organisatie | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 1      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 2      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging N      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
