---
title: 2c. Verdeelsleutel duur zorgproces obv Wlz-indicatie per leveringsvorm
weight: 5
---


## Toelichting

Deze verdeelsleutel betreft de procentuele verhouding van de duur van het zorgproces op basis van de Wlz-indicaties van cliënten bij een of meerdere vestigingen in de verslagperiode. De verdeelsleutel wordt berekend per leveringsvorm binnen de sector VV in de langdurige zorg, op organisatieniveau en per vestiging over een nader te bepalen periode.


## Uitgangspunten

* De duur van het zorgproces wordt bepaald op basis de Wlz-indicatie van een client.


## Berekening

De verdeelsleutel wordt als volgt berekend:

1. Selecteer alle clienten die in de verslagperiode beschikken over een Wlz-indicatie met zorgprofiel VV. 
2. Bepaal per client uit stap 1 de vestiging(en) en de leveringsvorm waaronder de Wlz-indicatie(s) valt of vallen. 
3. Bepaal per vestiging en voor de totale organisatie het aantal dagen dat het zorgproces duurde in de verslagperiode per leveringsvorm en de leveringsvormen tezamen.
4. Bereken het percentage, door per vestiging het aantal dagen per leveringsvorm te delen door het totaal aantal dagen van de leveringsvormen per vestiging tezamen en te vermeinigvuldigen met 100%.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj
| Verdeling duur Wlz-indicatie(s) VV:  |  Verblijf  | VPT | MPT |  PGB  | 
|----------------|--------|-----------|--------|--------|
| Totaal organisatie | Stap 4 | Stap 4    | Stap 4 |Stap 4|
| Vestiging 1      | Stap 4 | Stap 4    | Stap 4 |Stap 4| 
| Vestiging 2      | Stap 4 | Stap 4    | Stap 4 |Stap 4| 
| Vestiging N      | Stap 4 | Stap 4    | Stap 4 |Stap 4| 

