---
title: 2b. Verdeelsleutel duur zorgproces obv Wlz-indicatie per langdurige zorg sector
weight: 4
---

## Toelichting

Deze verdeelsleutel betreft de procentuele verhouding van de duur van de Wlz-indicaties bij een of meerdere vestigingen in de verslagperiode. De verdeelsleutel wordt berekend per sector binnen de langdurige zorg, op organisatieniveau en per vestiging over een nader te bepalen periode.


## Uitgangspunten

* De duur wordt bepaald op basis van de periode waarin het recht (ook wel legitimatie) bestaat om zorg te leveren.
* De periode wordt bepaald o.b.v. het aantal dagen dat ligt tussen de start- en einddatum van de Wlz-indicatie van een cliënt.


## Berekening

De verdeelsleutel wordt als volgt berekend:

1. Selecteer alle clienten die in de verslagperiode beschikken over een Wlz-indicatie. 
2. Bepaal per client uit stap 1 de vestiging(en) en de langdurige zorgsector(en) waaronder de Wlz-indicatie(s) valt of vallen. 
3. Bepaal per vestiging en voor de totale organisatie het aantal dagen dat de zorglegitimatie duurde in de verslagperiode per langdurige zorgsector en de langdurige zorgsectoren tezamen.
4. Bereken het percentage, door per vestiging het aantal dagen per sector te delen door het totaal aantal dagen van de sectoren per vestiging tezamen en te vermeinigvuldigen met 100%.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj
| Verdeling duur Wlz-indicatie(s) |  GGZ-B  | LG | LVG |  VG  | ZGAUD | ZGVIS | VV | GGZ-W |
|----------------|--------|-----------|--------|--------|--------|-----------|--------|---|
| Totaal organisatie | Stap 4 | Stap 4    | Stap 4 |Stap 4| Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 1      | Stap 4 | Stap 4    | Stap 4 |Stap 4| Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 2      | Stap 4 | Stap 4    | Stap 4 |Stap 4| Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging N      | Stap 4 | Stap 4    | Stap 4 |Stap 4| Stap 4 | Stap 4    | Stap 4 | Stap 4 |

