---
title: 2a. Verdeelsleutel duur zorglegitimatie per wet.
weight: 2
---
## SPARQL query
```
# Verdeelsleutel: 2a
# Parameters: -
# Ontologie: versie 2.0.0

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT 
	?vestiging
	(ROUND(100 * (SUM(?Wlz_dagen)/SUM(?dagen_zorgproces))) AS ?Wlz)
	(ROUND(100 * (SUM(?Wmo_dagen)/SUM(?dagen_zorgproces))) AS ?Wmo)
	(ROUND(100 * (SUM(?Zvw_dagen)/SUM(?dagen_zorgproces))) AS ?Zvw)
{
    { 
        SELECT DISTINCT ?vestiging ?zorgproces ?Wlz_dagen ?Wmo_dagen ?Zvw_dagen ?dagen_zorgproces
        {
            BIND ("2022-01-01"^^xsd:date AS ?start_periode)
            BIND ("2022-12-31"^^xsd:date AS ?eind_periode)

            #Bepaal filter voor definitie van overeenkomsten die geldig zijn voor personeelsleden
            VALUES ?type_indicatie
            { 
                onz-zorg:WlzIndicatie
                onz-zorg:WmoIndicatie
                onz-zorg:ZvwIndicatie
            }

            #selecteer de zorgprocessen die voldoen aan de inclusiecriteria
            ?zorgproces
                a onz-g:IntentionalProcess ;
                onz-g:definedBy ?indicatie ;
                onz-g:hasPerdurantLocation/onz-g:partOf* ?locatie ;
                onz-g:startDatum ?start_zorgproces .
                OPTIONAL {?zorgproces onz-g:eindDatum ?eind_zorgproces}
                FILTER(?start_zorgproces <= ?eind_periode && ((?eind_zorgproces >= ?start_periode) || (!BOUND(?eind_zorgproces))))
            
            #corrigeer zorgproces-periode zodat deze binnen de gevraagde periode valt
                BIND(IF(?start_zorgproces < ?start_periode, ?start_periode, ?start_zorgproces) AS ?start_zorgproces_corr)
                BIND(IF(?eind_zorgproces > ?eind_periode || !BOUND(?eind_zorgproces), ?eind_periode, ?eind_zorgproces) AS ?eind_zorgproces_corr)

            #Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
            {
                ?locatie 
                    a onz-org:Vestiging ;
                    rdfs:label ?vestiging .
            } UNION {
                #Includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?Organisatie .
                BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
            }
            #selecteer de bijbehordene indicatie, om unieke clienten te kunnen bepalen
            ?indicatie 
                a ?type_indicatie .

            #Bepaal aantal dagen zorg geleverd
            BIND ((360 * (YEAR(?eind_zorgproces_corr + "P1D"^^xsd:duration) - YEAR(?start_zorgproces_corr))) +
           (30 * (MONTH(?eind_zorgproces_corr + "P1D"^^xsd:duration) - MONTH(?start_zorgproces_corr))) +
           (DAY(?eind_zorgproces_corr + "P1D"^^xsd:duration) - DAY(?start_zorgproces_corr))
           AS ?dagen_zorgproces)
            
            #Per financieringsstroom
            BIND(IF(?type_indicatie = onz-zorg:WlzIndicatie, ?dagen_zorgproces, 0) AS ?Wlz_dagen)
            BIND(IF(?type_indicatie = onz-zorg:WmoIndicatie, ?dagen_zorgproces, 0) AS ?Wmo_dagen)
            BIND(IF(?type_indicatie = onz-zorg:ZvwIndicatie, ?dagen_zorgproces, 0) AS ?Zvw_dagen)
        }
    }
} 
GROUP BY ?vestiging
```