---
title: 1a. Verdeelsleutel goedgekeurd gedeclareerd bedrag per wet
weight: 1
---

## Toelichting

Deze verdeelsleutel betreft de procentuele verhouding van het bedrag dat door de zorgaanbieder per wet (Wlz, Zvw, Wmo en overig) voor zorg is gedeclareerd en vervolgens is goedgekeurd. De verdeelsleutel wordt berekend op organisatieniveau voor een bepaalde periode.


## Uitgangspunten

* Uitsluitend 'goedgekeurde declaraties' worden geïncludeerd.
* De goedgekeurde declaratie wordt geïncludeerd wanneer de datum waarop of periode waarin de zorg geleverd is, in de verslagperiode ligt.
* De indeling van een goedgekeurde declaratie in een bepaalde categorie vindt plaats op basis van de prestatiecode. Een beschrijving van de manier waarop dit plaatsvindt staat nader beschreven in de Algemene uitgangspunten in de paragraaf 'Het gebruik van declaraties'.


## Berekening

De verdeelsleutel wordt als volgt berekend:

1. Selecteer alle goedgekeurde declaraties waarvan de datum waarop of periode waarin de zorg geleverd is in de verslagperiode ligt. 
2. Bepaal op basis van stap 1 per declaratie het gedeclareerde bedrag en de wet waaronder de declaratie valt.
3. Bereken het gedeclareerde bedrag in Euro per wet en voor alle wetten tezamen.
4. Bereken het percentage, door het bedrag per wet en de categorie overig te delen door het bedrag van de wetten tezamen en te vermenigvuldigen met 100%.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatie-onderdeel | % gedeclareerd bedrag Wlz  | % gedeclareerd bedrag Zvw | % gedeclareerd bedrag Wmo | % gedeclareerd bedrag overig |
|---|---|---|---|---|
| Totaal organisatie | Stap 4 | Stap 4 | Stap 4 | Stap 4 |
