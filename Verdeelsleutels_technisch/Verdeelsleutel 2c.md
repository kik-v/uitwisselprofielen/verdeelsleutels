---
title: 2c. Verdeelsleutel duur zorgproces obv Wlz-indicatie per leveringsvorm
weight: 4
---
## SPARQL query
```
# Verdeelsleutel: 2c
# Parameters: -
# Ontologie: versie 2.0.0

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT 
	?vestiging
    (ROUND(100 * (SUM(?verblijf_dagen)/SUM(?dagen_zorgproces))) AS ?verblijf)
	(ROUND(100 * (SUM(?vpt_dagen)/SUM(?dagen_zorgproces))) AS ?vpt)
	(ROUND(100 * (SUM(?mpt_dagen)/SUM(?dagen_zorgproces))) AS ?mpt)
	(ROUND(100 * (SUM(?pgb_dagen)/SUM(?dagen_zorgproces))) AS ?pgb)
{
    { 
        SELECT DISTINCT ?vestiging ?zorgproces ?verblijf_dagen ?vpt_dagen ?mpt_dagen ?pgb_dagen ?dagen_zorgproces
        {
            BIND ("2022-01-01"^^xsd:date AS ?start_periode)
            BIND ("2022-12-31"^^xsd:date AS ?eind_periode)
            
            VALUES ?zorgprofiel {onz-zorg:4VV onz-zorg:5VV onz-zorg:6VV onz-zorg:7VV onz-zorg:8VV onz-zorg:9BVV onz-zorg:10VV }
            
            #Selecteer de zorgprocessen die voldoen aan de inclusiecriteria
            ?zorgproces
                a onz-zorg:NursingProcess ;
                onz-g:definedBy ?indicatie ;
                onz-g:hasPerdurantLocation/onz-g:partOf* ?locatie ;
                onz-g:startDatum ?start_zorgproces .
                OPTIONAL {?zorgproces onz-g:eindDatum ?eind_zorgproces}
                FILTER(?start_zorgproces <= ?eind_periode && ((?eind_zorgproces >= ?start_periode) || (!BOUND(?eind_zorgproces))))
            
            #corrigeer zorgproces-periode zodat deze binnen de gevraagde periode valt
            BIND(IF(?start_zorgproces < ?start_periode, ?start_periode, ?start_zorgproces) AS ?start_zorgproces_corr)
            BIND(IF(?eind_zorgproces > ?eind_periode || !BOUND(?eind_zorgproces), ?eind_periode, ?eind_zorgproces) AS ?eind_zorgproces_corr)

            #Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
            {
                ?locatie 
                    a onz-org:Vestiging ;
                    rdfs:label ?vestiging .
            } UNION {
                #Includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?Organisatie .
                BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
            }
            
            #Selecteer de bijbehorende indicatie, om unieke clienten te kunnen bepalen
            ?indicatie 
                onz-g:hasPart ?zorgprofiel ;
                onz-g:hasPart ?lv ;
                onz-g:isAbout ?client .
			
            ?client a onz-g:Human .
            ?lv a onz-zorg:Leveringsvorm .
            
            #Bepaal aantal dagen zorg geleverd
            BIND ((360 * (YEAR(?eind_zorgproces_corr + "P1D"^^xsd:duration) - YEAR(?start_zorgproces_corr))) +
           (30 * (MONTH(?eind_zorgproces_corr + "P1D"^^xsd:duration) - MONTH(?start_zorgproces_corr))) +
           (DAY(?eind_zorgproces_corr + "P1D"^^xsd:duration) - DAY(?start_zorgproces_corr))
           AS ?dagen_zorgproces)
            
            
            #Per leveringsstroom
            BIND(IF(?lv = onz-zorg:instelling, ?dagen_zorgproces, 0) AS ?verblijf_dagen)
            BIND(IF(?lv = onz-zorg:vpt, ?dagen_zorgproces, 0) AS ?vpt_dagen)
            BIND(IF(?lv = onz-zorg:mpt, ?dagen_zorgproces, 0) AS ?mpt_dagen)
            BIND(IF(?lv = onz-zorg:pgb, ?dagen_zorgproces, 0) AS ?pgb_dagen)
        }
    }
} 
GROUP BY ?vestiging
```