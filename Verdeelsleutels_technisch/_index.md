---
title: Verdeelsleutels technisch
Weight: 3
---
# Verdeelsleutels

Er zijn momenteel meerdere verdeelsleutels, namelijk:
* [1a. Verdeelsleutel gedeclareerd bedrag per wet](/Verdeelsleutels_technisch/Verdeelsleutel%201a.md)
* [1b. Verdeelsleutel gedeclareerd bedrag per langdurige zorg sector](/Verdeelsleutels_technisch/Verdeelsleutel%201b.md)
* [2a. Verdeelsleutel duur traject per wet](/Verdeelsleutels_technisch/Verdeelsleutel%202a.md)
* [2b. Verdeelsleutel duur traject per wet](/Verdeelsleutels_technisch/Verdeelsleutel%202b.md)
* [2c. Verdeelsleutel duur zorgproces obv Wlz-indicatie per leveringsvorm](/Verdeelsleutels_technisch/Verdeelsleutel%202c.md)