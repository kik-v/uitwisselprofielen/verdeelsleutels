---
title: 2b. Verdeelsleutel duur zorgproces obv Wlz-indicatie per langdurige zorg sector
weight: 3
---
## SPARQL query
```
# Verdeelsleutel: 2b
# Parameters: -
# Ontologie: versie 2.0.0

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT 
	?vestiging
	(ROUND(100 * (SUM(?ggzb_dagen)/SUM(?dagen_zorgproces))) AS ?GGZ_B)
    (ROUND(100 * (SUM(?lg_dagen)/SUM(?dagen_zorgproces))) AS ?LG)
    (ROUND(100 * (SUM(?lvg_dagen)/SUM(?dagen_zorgproces))) AS ?LVG)
    (ROUND(100 * (SUM(?vg_dagen)/SUM(?dagen_zorgproces))) AS ?VG)
    (ROUND(100 * (SUM(?vv_dagen)/SUM(?dagen_zorgproces))) AS ?VV)
    (ROUND(100 * (SUM(?zgaud_dagen)/SUM(?dagen_zorgproces))) AS ?ZGAUD)
    (ROUND(100 * (SUM(?zgvis_dagen)/SUM(?dagen_zorgproces))) AS ?ZGVIS)
{
    {
        SELECT DISTINCT ?vestiging ?zorgproces ?ggzb_dagen ?lg_dagen ?lvg_dagen ?vg_dagen ?vv_dagen ?zgaud_dagen ?zgvis_dagen ?dagen_zorgproces
        {
            BIND ("2022-01-01"^^xsd:date AS ?start_periode)
            BIND ("2022-12-31"^^xsd:date AS ?eind_periode)

            #selecteer de zorgprocessen die voldoen aan de inclusiecriteria
            ?zorgproces
                a onz-zorg:NursingProcess ;
                onz-g:definedBy ?indicatie ;
                onz-g:hasPerdurantLocation/onz-g:partOf* ?locatie ;
                onz-g:startDatum ?start_zorgproces .
                OPTIONAL {?zorgproces onz-g:eindDatum ?eind_zorgproces}
                FILTER(?start_zorgproces <= ?eind_periode && ((?eind_zorgproces >= ?start_periode) || (!BOUND(?eind_zorgproces))))
            
            #corrigeer zorgproces-periode zodat deze binnen de gevraagde periode valt
                BIND(IF(?start_zorgproces < ?start_periode, ?start_periode, ?start_zorgproces) AS ?start_zorgproces_corr)
                BIND(IF(?eind_zorgproces > ?eind_periode || !BOUND(?eind_zorgproces), ?eind_periode, ?eind_zorgproces) AS ?eind_zorgproces_corr)

            #Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
            {
                ?locatie 
                    a onz-org:Vestiging ;
                    rdfs:label ?vestiging .
            } UNION {
                #Includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?Organisatie .
                BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
            }
            #selecteer de bijbehordene indicatie, om unieke clienten te kunnen bepalen
            ?indicatie 
                a onz-zorg:WlzIndicatie ;
                onz-g:hasPart/onz-g:isAbout ?sector .

            ?sector
                a onz-zorg:LangdurigeZorgSector .

            #Bepaal aantal dagen zorg geleverd
            BIND ((360 * (YEAR(?eind_zorgproces_corr + "P1D"^^xsd:duration) - YEAR(?start_zorgproces_corr))) +
           (30 * (MONTH(?eind_zorgproces_corr + "P1D"^^xsd:duration) - MONTH(?start_zorgproces_corr))) +
           (DAY(?eind_zorgproces_corr + "P1D"^^xsd:duration) - DAY(?start_zorgproces_corr))
           AS ?dagen_zorgproces)
            
            #Per financieringsstroom
            BIND(IF(?sector = onz-zorg:GGZ-B, ?dagen_zorgproces, 0) AS ?ggzb_dagen)
            BIND(IF(?sector = onz-zorg:LG, ?dagen_zorgproces, 0) AS ?lg_dagen)
            BIND(IF(?sector = onz-zorg:LVG, ?dagen_zorgproces, 0) AS ?lvg_dagen)
            BIND(IF(?sector = onz-zorg:VG, ?dagen_zorgproces, 0) AS ?vg_dagen)
            BIND(IF(?sector = onz-zorg:VV, ?dagen_zorgproces, 0) AS ?vv_dagen)
            BIND(IF(?sector = onz-zorg:ZGAUD, ?dagen_zorgproces, 0) AS ?zgaud_dagen)
            BIND(IF(?sector = onz-zorg:ZGVIS, ?dagen_zorgproces, 0) AS ?zgvis_dagen)
        } 
    }
}
GROUP BY ?vestiging
```