---
title: Beslisboom
Weight: 1
---
## Toelichting op de keuzeboom verdeelsleutels
Onder volgt een stappenplan voor het toepassen van één of meerdere verdeelsleutels op de indicatoren personeelssamenstelling van het uitwisselprofiel Zorginstituut o.b.v. ontologie versie 2.0. Bij een aantal stappen wordt een voorbeeld gegeven. Het betreft de volgende keuzeboom:

```plantuml
(*) --> "Dient er een verdeelsleutel toegepast te worden?" as dient
dient --> "Ja" as dient_ja#pink
dient --> "Nee" as dient_nee#pink

dient_ja --> "Verdeelsleutel(s) o.b.v. definitie van:" as definieert
definieert --> "Zorgaanbieder" as zorgaanbieder#pink
definieert --> "KIK-V" as KIKV#pink

zorgaanbieder --> "Wat is de telling?" as telling_zorgaanbieder
telling_zorgaanbieder --> "n.t.b." as NTB_zorgaanbieder#pink
NTB_zorgaanbieder --> "Wat is de indeling?" as indeling_zorgaanbieder
indeling_zorgaanbieder --> "n.t.b." as NTB_zorgaanbieder_2#pink

NTB_zorgaanbieder_2#pink --> "Verdeelsleutel van zorgaanbieder" as VVZ#Yellow

KIKV --> "Wat is de telling?" as telling
telling --> "Bedrag declaraties (organisatieniveau)" as bedrag#pink
telling --> "Duur indicatie/beschikking (organisatieniveau, vestiging)" as traject#pink

bedrag --> "Wat is de indeling?" as indeling
traject --> "Wat is de indeling?" as indeling2

indeling --> "Wet (Wlz, Zvw, Wmo)" as wet#pink
indeling --> "Langdurige zorgsector (VV, VG, LG, …)" as LZ#pink

indeling2 --> "Wet (Wlz, Zvw, Wmo)" as wet2#pink
indeling2 --> "Langdurige zorgsector (VV, VG, LG, …)" as LZ2#pink
indeling2 --> "Langdurige zorgsector (VV)" as LZ3#pink

wet --> "[[/documentatie/Verdeelsleutels/1.1.0/Verdeelsleutels_functioneel/Verdeelsleutel%201a.md Verdeelsleutel 1a]]" as vs1a#yellow
wet2 --> "[[/documentatie/Verdeelsleutels/1.1.0/Verdeelsleutels_functioneel/Verdeelsleutels_functioneel/Verdeelsleutel%202a.md Verdeelsleutel 2a]]" as vs2a#yellow

LZ --> "[[/documentatie/Verdeelsleutels/1.1.0/Verdeelsleutels_functioneel/Verdeelsleutel%201b.md Verdeelsleutel 1b]]" as vs1b#yellow
LZ2 --> "[[/documentatie/Verdeelsleutels/1.1.0/Verdeelsleutels_functioneel/Verdeelsleutel%202b.md Verdeelsleutel 2b]]" as vs2b#yellow
LZ3 --> "[[/documentatie/Verdeelsleutels/1.1.0/Verdeelsleutels_functioneel/Verdeelsleutel%202c.md Verdeelsleutel 2c]]" as vs3b#yellow
```
## Stap 1 - Bepalen of een verdeelsleutel toegepast dient te worden
Bepaal allereerst of het noodzakelijk is om een verdeelsleutel toe te passen op de betreffende indicatoren. 

Dit hangt af van de situatie bij de zorgaanbieder. Met betrekking tot het uitwisselprofiel Zorginstituut is de doelgroep ‘cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10…’ (zie [Uitwisselprofiel Zorginstituut Openbaarmaking kwaliteitsindicatoren verpleeghuiszorg](https://kik-v-publicatieplatform.nl/documentatie/Uitwisselprofiel%20Zorginstituut%20Openbaarmaking%20kwaliteitsindicatoren%20verpleeghuiszorg/_/Documentatie/interoperabiliteit/organisatorisch.md)). Mochten de indicatoren reeds berekend zijn o.b.v. deze doelgroep dan is een verdeelsleutel niet nodig. Dit geldt ook wanneer de brongegevens (waarmee de indicator berekend wordt) uitsluitend betrekking hebben op de betreffende doelgroep.

## Stap 2 - Verdeelsleutel(s) o.b.v. definitie van zorgaanbieder of KIK-V

Wanneer bepaald is of er een verdeelsleutel noodzakelijk is om de indicatoren voor de betreffende doelgroep te berekenen, dient bepaald te worden of er een “eigen” verdeelsleutel wordt gehanteerd of één van de verdeelsleutels van KiK-V.

KiK-V kent de volgende 4 verdeelsleutels:

I. Op basis van bedrag aan declaraties*:
* 1a. Verdeelsleutel gedeclareerd bedrag per wet.
* 1b. Verdeelsleutel gedeclareerd bedrag per langdurige zorg sector.
  
*NB: deze verdeelsleutel kan niet berekend worden op basis van ontologie versie 2.0.*

II. Op basis van duur indicaties/zorgproces:
* 2a. Verdeelsleutel duur zorglegitimatie per wet.
* 2b. Verdeelsleutel duur zorgproces o.b.v. Wlz-indicatie per langdurige zorg sector.

*NB: Overige financieringen zoals GGZ, Jeugdzorg etc. zijn niet opgenomen in verdeelsleutels 1a, 1b, 2a en 2b. Dit leidt ertoe dat het aandeel aan andere financieringen zoals GGZ en Jeugdzorg proportioneel (naar rato) verdeeld is over de verschillende indelingen van de verdeelsleutel en daaruit volgend het resultaat van de indicator.*

## Stap 3 - Telling bepalen
Bepaal welke uitgangspunten er voor de gekozen verdeelsleutel gehanteerd gaan worden. Hierin kan een eigen keuze gemaakt worden of één of meerdere verdeelsleutels van KiK-V gebruikt worden. 

De verdeelsleutels van KiK-V gaan respectievelijk uit van de gedeclareerde bedragen (1a, 1b) enerzijds en de duur van Wlz-indicaties, Wmo-beschikkingen en/of Zvw-zorgtrajecten (2a, 2b) anderzijds. 

*NB: De verdeelsleutels o.b.v. declaraties vallen niet binnen de scope van ontologie versie 2.0.*

## Stap 4 - Indeling bepalen
Bepaal welke verdeelsleutel of combinatie van verdeelsleutels nodig is om een juiste toerekening te doen. Onderstaande tabel geeft de verdeling van de verdeelsleutels en de combinatie van verdeelsleutels t.a.v. de verdeelsleutels van KiK-V weer.

|                |                               |            |              |           |            |           |              |              |            |            |
|----------------|-------------------------------|------------|--------------|-----------|------------|-----------|--------------|--------------|------------|------------|
|     1a         |     Verdeling declaraties     |     Wlz    | Wlz          | Wlz       | Wlz        | Wlz       | Wlz          | Wlz          |     Zvw    |     Wmo    |
|     1b         |     Verdeling declaraties     |     VV*    |     GGZ-B    |     LG    |     LVG    |     VG    |     ZGAUD    |     ZGVIS    |     -      |     -      |
|     1a + 1b    |     Verdeling declaraties     |     VV*    |     GGZ-B    |     LG    |     LVG    |     VG    |     ZGAUD    |     ZGVIS    |     Zvw    |     Wmo    |
|     2a         |     Verdeling indicaties      |     Wlz    | Wlz          | Wlz       | Wlz        | Wlz       | Wlz          | Wlz          |     Zvw    |     Wmo    |
|     2b         |     Verdeling indicaties      |     VV*    |     GGZ-B    |     LG    |     LVG    |     VG    |     ZGAUD    |     ZGVIS    |     -      |     -      |
|     2a + 2b    |     Verdeling indicaties      |     VV*    |     GGZ-B    |     LG    |     LVG    |     VG    |     ZGAUD    |     ZGVIS    |     Zvw    |     Wmo    |

*Scope van de ODB (cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10).*

## Stap 5 - Verdeelsleutel toepassen
Bepaal per indicator of, en op welke manier de verdeelsleutel op de teller en de noemer toegepast dienen te worden. In onderstaande tabel staat per indicator van de ODB personeel aangegeven of de verdeelsleutel toepasbaar is op de teller, de noemer of op beide. Vervolgens volgt een getallenvoorbeeld voor een aantal indicatoren.

|      Indicator OBD personeel                                                        |      Teller                                                |      Noemer                                                                |      Resultaat indicator zonder    verdeel-sleutel                                 |      Verdeelsleutel toepasbaar    op:     |      Toelichting                                                                                                                                                                                                                                                                                                                                    |
|-------------------------------------------------------------------------------------|------------------------------------------------------------|----------------------------------------------------------------------------|------------------------------------------------------------------------------------|-------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|     2.1.1 Aantal personeelsleden                                                    |     Aantal personeelsleden (naar rato)                     |     n.v.t.                                                                 |     Aantal   personeelsleden (naar rato)                                           |     teller                                |     Resultaat indicator is absoluut getal.                                                                                                                                                                                                                                                                                                          |
|     2.1.2 Aantal fte                                                                |     Aantal   fte per week                                  |     n.v.t.                                                                 |     Aantal   fte per week                                                          |     teller                                |     Resultaat indicator is absoluut getal.            Bij toepassing op indicator wordt indicator:   aantal fte VV per week.                                                                                                                                                                                                                        |
|     2.1.3 Percentage personeel met een   arbeidsovereenkomst voor bepaalde tijd     |     Aantal   arbeidsovereenkomsten voor bepaalde tijd      |     Aantal   arbeidsovereenkomsten                                         |     %   Aantal arbeidsovereenkomsten bepaalde tijd t.o.v. arbeidsovereenkomsten    |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.1.4 Percentage inzet   uitzendkrachten personeel niet in loondienst (PNIL)    |     Aantal   uren inhuur en uitzend                        |     Aantal   uren arbeid, inhuur, uitzend                                  |     %   aantal uren i+u t.o.v. aantal uren a+i+u                                   |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.1.5 Percentage kosten personeel niet in   loondienst (PNIL)                   |     n.v.t                                                  |     n.v.t                                                                  |     n.v.t                                                                          |     n.v.t                                 |     Indicator is geen onderdeel van RFP, omdat   financiën geen onderdeel uitmaakt van ontologie v2.0.                                                                                                                                                                                                                                              |
|     2.1.6 Gemiddelde contractomvang                                                 |     Indicator   2.1.2:            Aantal   fte per week    |     Indicator   2.1.1:            Aantal   personeelsleden (naar rato)     |     Aantal   fte per week t.o.v. aantal personeelsleden (naar rato)                |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een verhouding betreft.                                                                                                                                                                                                                                                                         |
|     2.2.1 Percentage fte per niveau                                                 |     Aantal   fte per week per niveau (9 tellers)           |     Indicator   2.1.2: Aantal fte per week                                 |     Aantal   fte per week per niveau t.o.v.     Aantal   fte per week              |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een verhouding betreft.                                                                                                                                                                                                                                                                         |
|     2.2.2 Aantal stagiairs                                                          |     Aantal stagiars                                        |     n.v.t.                                                                 |     Aantal   stagiars                                                              |     teller                                |     Indicatorwaarde is absoluut getal.                                                                                                                                                                                                                                                                                                              |
|     2.2.3 Aantal vrijwilligers                                                      |     Aantal vrijwilligers                                   |     n.v.t.                                                                 |     Aantal   vrijwilligers                                                         |     teller                                |     Indicatorwaarde is absoluut getal.                                                                                                                                                                                                                                                                                                              |
|     2.3.1 Ziekteverzuimpercentage                                                   |     Aantal   ziektedagen                                   |     Aantal kalenderdagen                                                   |     %   ziektedagen t.o.v. kalenderdagen                                           |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.3.2 Verzuimfrequentie                                                         |     Aantal   ziekmeldingen                                 |     Indicator   2.1.1:            Aantal   personeelsleden (naar rato)     |     Aantal   ziekmeldingen t.o.v. aantal personeelsleden (naar rato)               |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een verhouding betreft.                                                                                                                                                                                                                                                                         |
|     2.4.1 Percentage instroom                                                       |     Aantal   personeelsleden                               |     Gem.   aantal personeelsleden                                          |     %   aantal personeelsleden t.o.v. gem. aantal personeelsleden                  |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.4.2 Percentage uitstroom                                                      |     Aantal   personeelsleden                               |     Gem.   aantal personeelsleden                                          |     %   aantal personeelsleden t.o.v. gem. aantal personeelsleden                  |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.4.3 Percentage doorstroom   kwalificatieniveau                                |     Aantal   personeelsleden                               |     Gem.   aantal personeelsleden                                          |     %   aantal personeelsleden t.o.v. gem. aantal personeelsleden                  |     teller   + noemer                     |     Resultaat indicator blijft gelijk , omdat   het een percentage betreft.                                                                                                                                                                                                                                                                         |
|     2.5.1 Aantal fte zorg cliënt ratio                                              |     Indicator   2.1.2:            Aantal   fte per week    |     Aantal clientdagen VV4-VV10                                            |     Aantal   fte per week t.o.v. aantal clientdagen                                |     teller                                |     Noemer betreft reeds doelgroep Wlz VV4-VV10.           Op de teller van deze indicator (indicator   2.1.1) is de verdeelsleutel nog niet eerder toegepast.           Bij toepassing op teller wordt indicator:   aantal fte per week VV (en eventueel andere financieringsstromen naar rato)   t.o.v. aantal clientdagen Wlz VV4-VV10.          |

### Voorbeeld berekening verdeelsleutel
Scope ODB personeel is ‘Cliënten met Wlz-indicatie met zorgprofiel VV4 tot en met VV10’. Het aantal personeelsleden van de totale organisatie is bijvoorbeeld 1.000. De verdeling op basis van indicaties/zorgproces is bijvoorbeeld:

|                |                             |     VV     |     GGZ-B    |     LG    |     LVG    |     VG    |     ZGAUD    |     ZGVIS    |     Zvw    |     Wmo    |
|----------------|-----------------------------|------------|--------------|-----------|------------|-----------|--------------|--------------|------------|------------|
|     2a + 2b    |     Verdeling indicaties    |     83%    |     5%       |     1%    |     1%     |     0%    |     0%       |     0%       |     5%     |     5%     |

In dit voorbeeld is de verdeelsleutel (voor VV) 0,83. De uitkomst van de indicator met verdeelsleutel is dan 830 (0,83 x 1.000). Zonder verdeelsleutel is deze 1.000. 

*NB: Mocht in dit voorbeeld de zorgaanbieder ook GGZ-gefinancierde zorg leveren die wordt uitgevoerd door een deel van de 1.000 personeelsleden, dan worden ‘deze personen’ proportioneel verdeeld over de uitkomst van de indicator. Ergo, in de uitkomst van de indicator van 830 zijn tevens personeelsleden meegerekend die GGZ-gefinancierde zorg leveren. In dit geval betreft dit dus tevens 83% van deze personeelsleden.*

### Voorbeeld met getallen m.b.t. indicatoren ODB personeel i.c.m. verdeelsleutel in het datastation

Uitgangspunten:
-   1.500 personeelsleden op organisatieniveau.
-   750 personeelsleden gehele verslagperiode in dienst, de rest een halve verslagperiode.
-   1.000 ingezette (ook gewerkte uren genoemd) uren fulltime per jaar (47 weken, 1 fte is 36 uren) per personeelslid op organisatieniveau. 250 inhuur en 300 uitzend.
-   1.250 arbeidsovereenkomsten, waarvan 250 voor bepaalde tijd.
-   Verdeelsleutel:
o   Aandeel Wlz VV4 – VV10: 80%;
o   Aandeel Wlz exclusief VV, Zvw, Wmo: 20%.

|     Indicator                                                                       |     Teller        zonder   verdeelsleutel              |     Noemer        zonder   verdeelsleutel    |     Resultaat   zonder verdeelsleutel    |     Teller        met   verdeelsleutel          |     Noemer     met   verdeelsleutel          |     Resultaat   met verdeelsleutel             |
|-------------------------------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------|------------------------------------------|-------------------------------------------------|----------------------------------------------|------------------------------------------------|
|     2.1.1 Aantal personeelsleden                                                    |     1.125     (750 * 1 + 750 * 0,5)                    |     nvt                                      |     1.125   personeels-leden             |     900                                         |     nvt                                      |     900                                        |
|     2.1.2 Aantal fte                                                                |     665     ((750   * 1.000 + 750 * 500) / 36 / 47)    |     nvt                                      |     665     fte   / week                 |     532                                         |     nvt                                      |     532        fte   / week                    |
|     2.1.3 Percentage personeel met een   arbeidsovereenkomst voor bepaalde tijd     |     250                                                |     1.250                                    |     20   %     (250   / 1.250 * 100%)    |     200                                         |     1.000                                    |     20   %     (200   / 1.000 * 100%)          |
|     2.1.4 Percentage inzet   uitzendkrachten personeel niet in loondienst (PNIL)    |     550        (250   + 300)                           |     1.550     (250 + 300 + 1.000)            |     35   %      (550   / 1.550)          |     440                                         |     1.240                                    |     35   %      (440   / 1.240 * 100%)         |
|     2.1.5 Percentage kosten personeel niet in   loondienst (PNIL)                   |     nvt                                                |     nvt                                      |     nvt                                  |     nvt                                         |     nvt                                      |     nvt                                        |
